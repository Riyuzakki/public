# coding=utf-8
import logging
import types
from urlparse import urlparse, urljoin
from flask import request, url_for, render_template, render_template_string, abort, redirect, flash
from flask import session
from flask.ext.wtf import Form
from app import db, app
from os.path import isfile
from flask.ext.babel import gettext, ngettext
from werkzeug.routing import BuildError
from werkzeug.datastructures import MultiDict
from functools import wraps


log = logging.getLogger('app.mod_order.service')


class BaseForm(Form):
	extended_params = {}
	# Например, extended_params = {'enctype': 'multipart/form-data'}

	def get_present_object(self):
		present_object = PresentObject(self.data)
		# self.populate_obj(present_object)
		return present_object

	def __iter__(self):
		field_order = getattr(self, 'field_order', ['*'])
		temp_fields = []
		for name in field_order:
			if name == '*':
				temp_fields.extend([f for f in self._unbound_fields if f[0] not in field_order])
			else:
				temp_fields.append([f for f in self._unbound_fields if f[0] == name][0])
		self._unbound_fields = temp_fields
		return super(BaseForm, self).__iter__()


class PresentObject(dict):
	"""This class behavior like a dict and object.
	Used to get data from model, set it back and post in views and forms.
	"""

	def __setattr__(self, key, value):
		self[key] = value

	def __getattr__(self, item):
		try:
			return self[item]
		except KeyError:
			raise AttributeError('%s has no attrbute %s' % (self, item))

	def copy(self):
		return PresentObject(self)


class BaseView(object):
	"""Class for render templates or get error then something wrong

	"""

	_error = None
	_template = 'base.html'
	template = None
	_context = {}
	_redirect = None
	title = None

	def __init__(self, template=None, context=None):
		self.set_context(context=context)
		self.set_template(template=template)

	def show_message(self, message, category='message'):
		"""Show message on this page (or next page then redirect)

		:param message: message content string. May be translated, but NOT lazy!
		:param category: string for indent group of message. On HTML present as container css-class usually
		:return:
		"""
		flash(message=message, category=category)

	def show_error(self, message):
		self.show_message(message=message, category='error')

	def set_error(self, error):
		if error in [200, 201]:
			self._error = None
		elif error in [404, 304]:
			self._error = error
		else:
			self._error = 503

	def set_redirect(self, target=True, **url_params):
		"""Set view._redirect.

		:param target:
		* endpoint - redirect to werkzeug endpoint
		* URL - redirect target
		* Wrong URL - target sets default
		* True - target sets default
		* False - undo redirect
		:param url_params: dict of url_for values
		:return:
		"""
		if isinstance(target, basestring):
			try:
				url_params = url_params if isinstance(url_params, dict) else dict()
				target = url_for(target, **url_params)
			except BuildError:
				pass
		if isinstance(target, basestring) and is_safe_url(target):
			self._redirect = redirect(target)
		elif target:
			self._redirect = redirect(get_redirect_target())
		else:
			self._redirect = None

	def set_context_or_404(self, context):
		if type(context) not in (dict, PresentObject) \
				or len(context) == 0:
			self.set_error(404)
		else:
			self.set_context(context=context)

	def set_context(self, context=None):
		if not context: context = dict()
		self._context = context

	def set_template(self, template):
		self._template = template or getattr(self, 'template', False) or self._template or BaseView._template

	def render(self):
		if self._redirect is not None:
			return self._redirect
		if self._error is not None:
			return abort(self._error)
		context = self._context or dict()
		if isinstance(self.title, basestring):
			context['title'] = self.title
		if isfile(app.template_folder + '/' + self._template):
			return render_template(self._template, **context)
		elif type(self._template) == str and len(self._template) > 0:
			return render_template_string(self._template, **context)
		return abort(503)


class BaseFormView(BaseView):
	_form = None
	_template = 'base_form.html'
	_loaded_state = False
	cancel_url = None
	cancel_text = None
	submit_text = None

	def __init__(self, form_class=None, template=None, context=None):
		super(BaseFormView, self).__init__(template, context)
		self._form_class = form_class or getattr(self, 'form_class', BaseForm)

	@property
	def form_class(self):
		if issubclass(self._form_class, Form):
			return self._form_class
		else:
			return BaseForm

	@property
	def form(self):
		if not self._form:
			self._form = self.form_class()
		return self._form

	def render(self):
		if isinstance(self.cancel_url, basestring):
			self._context['cancel_url'] = self.cancel_url
		if isinstance(self.cancel_text, basestring):
			self._context['cancel_text'] = self.cancel_text
		if isinstance(self.submit_text, basestring):
			self.form.submit_text = self.submit_text
		self._context['form'] = self.form
		return super(BaseFormView, self).render()

	def save_state(self, sign=''):
		if self.form.is_submitted():
			session['_'.join((self.__class__.__name__, sign))] = self.form.get_present_object()

	def load_state(self, sign='', fake_submit=False):
		saved_data = session.get('_'.join((self.__class__.__name__, sign)), None)
		if isinstance(saved_data, dict):
			if fake_submit:
				self._loaded_state = True
				self._form = self.form_class(formdata=MultiDict(saved_data),
											obj=PresentObject(saved_data),
											csrf_enabled=False)
			else:
				self._form = self.form_class(obj=PresentObject(saved_data))

	def clear_state(self, sign=''):
		session.pop('_'.join((self.__class__.__name__, sign)))

	def action(self):
		raise NotImplemented

	def is_submitted(self):
		return self.form.is_submitted()

	def is_complete(self):
		if self._loaded_state:
			return self.form.validate()
		return self.form.validate_on_submit()

	def done(self):
		if self.is_complete():
			return self.action()
		return False


class BaseService(object):
	"""Base class for services. It`s incarnates some magic
	"""

	@staticmethod
	def hook(weight=0):
		"""Декоратор для создания методов-хуков, вызывающих свои переоределения из дочерних классов.
		Декорироваться должны как метод базового класса, так и методы в дочерних классах.
		Все хуки дожны быть классовыми методами (@classmethod)
		Пример:

		class Serv(BaseService):
			@classmethod
			@BaseService.hook()
			def set_price(cls, item):
				item.price = 2

		class ServPlug1(Serv):
			@classmethod
			@Serv.hook(weight=1)
			def set_price(cls, item):
				item.price = item.price * 2

		class ServPlug2(Serv):
			@classmethod
			@Serv.hook(weight=0)
			def set_price(cls, item):
				item.price += 2

		>>> item = PresentObject()
		>>> Serv.set_price(item)
		>>> item.price
		8
		>>> item1 = PresentObject()
		>>> ServPlug1.set_price(item1)
		>>> item1.price
		8
		>>> item2 = PresentObject()
		>>> ServPlug2.set_price(item2)
		>>> item2.price
		8

		:param weight: задает порядок исполнения дочерних методов
		:return:
		"""
		def hook(func):
			from functools import wraps

			func.weight = weight
			@wraps(func)
			def f(cls, *args, **kwargs):
				hook_name = func.__name__
				called_parent = hook_name not in cls.__mro__[1].__dict__
				func_hook = getattr(cls, hook_name).im_func
				called_as_hook = getattr(func_hook, 'called_as_hook', False)
				try:
					setattr(func_hook, 'called_as_hook', False)
				except AttributeError:
					pass
				if called_parent or called_as_hook:
					func(cls, *args, **kwargs)
					hooks = list()
					for subclass in cls.__subclasses__():
						if hook_name in subclass.__dict__:
							hooks.append(getattr(subclass, hook_name))
					hooks.sort(key=lambda i: i.weight)
					for hook in hooks:
						hook.im_func.called_as_hook = True
						hook(*args, **kwargs)
				else:
					getattr(cls.__mro__[1], hook_name).__call__(*args, **kwargs)
			return f
		return hook

	@staticmethod
	def id_from_multiarg(multiarg):
		try:
			multiarg = multiarg.id
		except (TypeError, AttributeError):
			pass
		try:
			multiarg = int(multiarg)
		except (TypeError, ValueError):
			pass
		if type(multiarg) is int:
			return multiarg
		return None

	@classmethod
	def load_model_or_404(cls, model_class=None, model_getter=None, id_name='model_id'):
		"""Декоратор загрузки модели по ID.

		:param model_class:
		Класс модели. Если задан, модель загружается через этот класс.
		Возвращается PresentObject, полученный из модели.
		:param model_getter:
		Функция получения модели из сервиса. Может задаваться строкой с именем или методом напрямую.
		Возвращается то, что вернет метод. По-умолчанию 'get'.
		:param id_name:
		Имя атрибута, содержащего ID модели, который будет передаваться при вызове.
		:return:
		"""
		def real_decorator(fn):
			@wraps(fn)
			def decorator(*args, **kwargs):
				model_id = kwargs.pop(id_name, None)
				model_getter_ = model_getter
				if isinstance(model_class, DB_BaseModel):
					db_model = model_class.get_by_id(model_id)
					if isinstance(db_model, model_class):
						model = db_model.get_present_object()
						return fn(model, *args, **kwargs)
				if model_getter_ is None:
					model_getter_ = 'get'
				if isinstance(model_getter_, (str, unicode)):
					model_getter_ = getattr(cls, model_getter_, False)
				if type(model_getter_) in method_types():
					model = model_getter_(model_id)
					if model:
						return fn(model, *args, **kwargs)
				return abort(404)
			return decorator
		return real_decorator


class DB_BaseModel(db.Model):
	"""Base class for SQLAlchemy models
	"""

	__abstract__ = True

	id = db.Column(db.Integer, primary_key=True)
	date_created = db.Column(db.DateTime, default=db.func.current_timestamp())
	date_modified = db.Column(db.DateTime, default=db.func.current_timestamp(),
							  onupdate=db.func.current_timestamp())

	@classmethod
	def get_by_id(cls, object_id):
		return cls.query.get(object_id)

	@classmethod
	def get_by_filter(cls, **kwargs):
		return cls.query.filter_by(**kwargs).first()

	@classmethod
	def get_all(cls):
		return cls.query.all()

	@classmethod
	def get_all_by_filter(cls, **kwargs):
		return cls.query.filter_by(**kwargs).all()

	@classmethod
	def get_count_by_filter(cls, **kwargs):
		return cls.query.filter_by(**kwargs).count()

	@classmethod
	def get_all_by_filter_ordered(cls, order_by_field='id', **kwargs):
		return cls.query.filter_by(**kwargs).order_by(getattr(cls, order_by_field, None)).all()

	def __init__(self, **kwargs):
		"""Set Model instance properties and add instance to Alchemy session
		sqlalchemy/ext/declarative/base.py:508 (_declarative_constructor method)

		:param kwargs: arguments named like Model properties
		:return:
		"""
		cls_ = type(self)
		for k in kwargs:
			if k not in dir(cls_):
				raise TypeError(
					"%r is an invalid keyword argument for %s" %
					(k, cls_.__name__))
			setattr(self, k, kwargs[k])
		db.session.add(self)

	def __unicode__(self):
		return self.__str__()

	def __str__(self):
		attrs = db.class_mapper(self.__class__).attrs  # show also relationships
		if 'name' in attrs:
			return self.name
		elif 'code' in attrs:
			return self.code
		else:
			return "<%s(id=%s)>" % (self.__class__.__name__, self.id)

	def commit(self):
		"""Save instance on disk.
		In the case of SQLAlchemy method saving all changes in session.

		:return:
		"""
		db.session.add(self)
		try:
			db.session.commit()
		except Exception as ex:
			db.session.rollback()
			log.error(ex.message)
			if app.config.get('DEBUG', False):
				raise
		return self.id

	def delete(self):
		db.session.delete(self)
		db.session.commit()

	def get_present_object(self):
		"""Get model data for present in views and forms

		:return:
		PresentObject with copied model attributes
		"""
		present_object = PresentObject()
		id = self.id  # WOW! without it present_object returns empty
		for item in self.__dict__:
			if item == '_sa_instance_state': continue
			present_object[item] = getattr(self, item)
		return present_object

	def populate_from_present_object(self, present_object):
		"""Set attributes from PresentObject

		:param present_object: PresentObject or dict instance with data named as model attributes
		"""
		for key, val in present_object.iteritems():
			setattr(self, key, val)


def is_safe_url(target):
	if not isinstance(target, basestring): return False
	ref_url = urlparse(request.host_url)
	test_url = urlparse(urljoin(request.host_url, target))
	return not (not (test_url.scheme in ('http', 'https')) or not (ref_url.netloc == test_url.netloc))


def get_redirect_target():
	"""Get target to redirect after POST request

	RETURNS:
	* target url
	"""
	for target in request.values.get('next'), request.referrer:
		if not target:
			continue
		if is_safe_url(target):
			return target
	return url_for('index')


def method_types():
	yield types.MethodType
	yield types.FunctionType
	yield types.LambdaType


def calculate_discount(total, fix_discount=0, percent_discount=0):
	discounted_total = total * (1 - percent_discount / 100.0) - fix_discount
	return discounted_total if discounted_total > 0 else 0
