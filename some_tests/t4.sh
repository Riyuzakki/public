#!/bin/bash

# Получение десяти самых активных ip-адресов из access.log

awk '{print $1}' access.log | uniq -c | sort -gr | head -n10
