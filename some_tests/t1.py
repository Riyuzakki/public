# coding=utf-8
"""
Есть два списка разной длины. В первом содержатся ключи, а во втором значения.
Напишите функцию, которая создаёт из этих ключей и значений словарь. Если ключу не хватило значения,
в словаре должно быть значение None. Значения, которым не хватило ключей, нужно игнорировать.
"""


def create_dict_more_readable_code(keys, vals):
    assert isinstance(keys, (list, tuple))
    assert isinstance(vals, (list, tuple))
    target_dict = {}
    for i in xrange(len(keys)):
        try:
            target_dict[keys[i]] = vals[i]
        except IndexError:
            target_dict[keys[i]] = None
    return target_dict


def create_dict_middle_variant(keys, vals):
    assert isinstance(keys, (list, tuple))
    assert isinstance(vals, (list, tuple))
    if len(keys) > len(vals):
        return dict(map(lambda k,v: (k,v), keys, vals))
    else:
        return dict(zip(keys, vals))


def create_dict_with_python_power(keys, vals):
    assert isinstance(keys, (list, tuple))
    assert isinstance(vals, (list, tuple))
    return dict( filter( lambda x: x[0] is not None, map(lambda k,v: (k,v), keys, vals) ) )


if __name__ == '__main__':
    keys = [1, 2, 3, 4, 5]
    vals = ['q', 'w', 'e', 'r']

    print( create_dict_more_readable_code(keys, vals) )
    print( create_dict_middle_variant(keys, vals) )
    print( create_dict_with_python_power(keys, vals) )
    print

    vals += ('t',)

    print( create_dict_more_readable_code(keys, vals) )
    print( create_dict_middle_variant(keys, vals) )
    print( create_dict_with_python_power(keys, vals) )
    print

    vals += ('y',)

    print( create_dict_more_readable_code(keys, vals) )
    print( create_dict_middle_variant(keys, vals) )
    print( create_dict_with_python_power(keys, vals) )
