# coding=utf-8
"""
Получение десяти самых активных ip-адресов из access.log
"""


if __name__ == "__main__":
    ip_dict = {}
    log_file = open('access.log')
    for log_line in log_file:
        ip = log_line.split(' ')[0]
        if ip in ip_dict:
            ip_dict[ip] += 1
        else:
            ip_dict[ip] = 1
    ip_sorted = sorted(ip_dict.items(), key=lambda x: x[1], reverse=True)
    for ip_count in ip_sorted[:10]:
        print '%r\t%s' % (ip_count[1], ip_count[0])
