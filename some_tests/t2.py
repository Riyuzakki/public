# coding=utf-8
"""
В системе авторизации есть ограничение: логин должен начинаться с латинской буквы, состоять из латинских букв, цифр,
точки и минуса, но заканчиваться только латинской буквой или цифрой; минимальная длина логина — один символ,
максимальная — 20. Напишите код, проверяющий соответствие входной строки этому правилу.
"""

import re
import string


class LoginCheckerRegexp(object):
    """Алгоритм с использованием регулярных выражений.
    Краток. Легко читаем. Быстр.
    Но преимущество в скорости может быть нивелировано, если понадобится изменять шаблон в ходе выполнения.
    """

    _reg_checker = re.compile('^[a-zA-Z]([A-Za-z0-9.-]{0,18}[a-zA-Z])?$')

    @classmethod
    def __call__(cls, login):
        assert isinstance(login, basestring)
        if cls._reg_checker.match(login):
            return True
        return False


class LoginCheckerPython(object):
    """Алгоритм посимвольной проверки.
    Гибок. Позволяет вмешиваться в порядок проверки строки, за счет чего можно увеличить скорость работы,
    если знать параметры проверяемой выборки.
    """

    symbols = '.-'
    allowed_start_chars = string.ascii_letters
    allowed_end_chars = allowed_start_chars
    allowed_middle_chars = ''.join((string.ascii_letters, string.digits, symbols))

    @classmethod
    def __call__(cls, login):
        assert isinstance(login, basestring)
        if not(0 < len(login) <= 20):
            return False
        if login[:1] not in cls.allowed_start_chars:
            return False
        if login[-1:] not in cls.allowed_end_chars:
            return False
        if len(filter(lambda c: c not in cls.allowed_middle_chars, login[1:-1])) > 0:
            return False
        return True


if __name__ == '__main__':
    doc = """
    -h,--help     Print this help
    -r,--regexp   Use regexp for search (default)
    -p,--python   Use algorithm without regexp
    -t,--timer    Show execution time
    """
    import sys
    import time
    import getopt

    login_checker = LoginCheckerRegexp()
    show_timer = False

    try:
        opts, args = getopt.getopt(sys.argv[1:], 'hrpt', ['help', 'regexp', 'python', 'timer'])
    except getopt.GetoptError as err:
        print err
        print doc
        sys.exit(2)
    for opt, arg in opts:
        if opt in ('-h', '--help'):
            print doc
            sys.exit()
        if opt in ('-t', '--timer'):
            show_timer = True
        elif opt in ('-p', '--python'):
            login_checker = LoginCheckerPython()

    start_time = time.time()

    while True:
        print '>>> ',
        line = sys.stdin.readline()
        if not line:
            break
        if login_checker(line.strip()):
            print 'Yes'
        else:
            print 'No'

    if show_timer:
        print
        print 'Use %s class' % login_checker.__class__.__name__
        print 'Execution time: ',
        print time.time() - start_time
