import inspect


class Service(object):
	"""Test commint...
	"""

	@classmethod
	def implement_hooks(cls):
		"""It`s a magic
		"""
		stack = inspect.stack()
		hook_name = stack[1][3]
		hook_args = stack[1][0].f_locals
		slf = hook_args.pop('self')
		for plugin in cls.__subclasses__():
			if hook_name in dir(plugin):
				old_class = slf.__class__
				slf.__class__ = plugin
				getattr(plugin, hook_name).__call__(slf, **hook_args)
				slf.__class__ = old_class

	@staticmethod
	def call_unbound_method(method_, instance_, class_):
		def wrap_call_unbound_method(*args, **kwargs):
			old_class = instance_.__class__
			instance_.__class__ = class_
			output = method_(instance_, *args, **kwargs)
			instance_.__class__ = old_class
			return output
		return wrap_call_unbound_method

	def __getattr__(self, item):
		"""It`s a better magic
		"""
		try:
			return object.__getattribute__(self, item)
		except AttributeError:
			for plugin in type(self).__subclasses__():
				if item in dir(plugin):
					old_class = self.__class__
					self.__class__ = plugin
					output = getattr(plugin, item)
					self.__class__ = old_class
					if inspect.ismethod(output):
						return Service.call_unbound_method(output, self, plugin)
					return output
		raise AttributeError

	def __se1tattr__(self, key, value):
		for plugin in type(self).__subclasses__():
			if key in dir(plugin):
				old_class = self.__class__
				self.__class__ = plugin
				object.__setattr__(self, key, value)
				self.__class__ = old_class


class Base(Service):
	def base_func(self, t):
		self.implement_hooks()


class OtherBase(Service):
	def other_base_func(self):
		self.implement_hooks()


class Plugin1(Base, OtherBase):

	name = 'name'

	def base_func(self, t):
		print 'Plug1 of Base %s' % t

	def other_base_func(self):
		print 'Plug1 of Other Base'


class Plugin2(Base):

	lastname = 'lastname'

	def base_func(self, t):
		print 'Plug2 of Base %s' % t

	def other_base_func(self):
		print 'Plug2 of Other Base'

	def plugin_func(self):
		return self.lastname

print '\ncheck Base\n'

b = Base()
b.base_func('123')
print b.name
print b.lastname
f = b.plugin_func
print f()

print '\ncheck Other Base\n'

ob = OtherBase()
ob.other_base_func()
print ob.name
print ob.lastname
