/**
 * Created by kriss on 23.05.14.
 */

utmtrace_StoreUtm = function () {
	if (window.location.search.indexOf('utm_source') == -1) return;
	jQuery.getJSON('http://jsonip.appspot.com/', function (data) {
		var query = window.location.search.substring(1);
		var qvars = query.split("&");
		var cur_date = new Date();
		var new_utm = {ip: data.ip, date: cur_date};
		for (var i = 0; i < qvars.length; i++) {
			var pair = qvars[i].split("=");
			if (pair[0].indexOf('utm_') == 0) {
				new_utm[pair[0]] = pair[1];
			}
		}
		var stored_utms = jQuery.localStorage.get('utmtrace_storedutms');
		if (stored_utms == null) stored_utms = new Array();
		var utm_isnot_stored = true;
		for (var i = 0; i < stored_utms.length; i++) {
			if (JSON.stringify(stored_utms[i]) === JSON.stringify(new_utm)) {
				utm_isnot_stored = false;
				break
			}
		}
		if (utm_isnot_stored) {
			stored_utms.push(new_utm);
			jQuery.localStorage.set('utmtrace_storedutms', stored_utms);
		}
	});
};

utmtrace_SendUtm = function() {
	jQuery('form#download').submit(function(){
		jQuery('#download-form-email-error').hide();
		var stored_utms = jQuery.localStorage.get('utmtrace_storedutms');
		var email = jQuery(this).find('input[name=email]').val();
		var is_email = function (email) {
			var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,9})+$/;
			return regex.test(email);
		}
		if ( !is_email(email) ) {
			jQuery('#download-form-email-error').show();
			return false;
		}
		if (stored_utms == null) return;
		var data = {email: email,
					utms: stored_utms};
		var xhr = new XMLHttpRequest();
		xhr.open('POST', '/track/utm', true);
		xhr.setRequestHeader('Content-Type', 'application/json; charset=UTF-8');
		xhr.send(JSON.stringify(data));
		xhr.onloadend = function () {
		};
	});
};

utmtrace_FormSettings = function(){
	jQuery('#download-form-email-error').hide();
	var OSName="Unknown OS";
	if (navigator.appVersion.indexOf("Win")!=-1) OSName="Windows";
	if (navigator.appVersion.indexOf("Mac")!=-1) OSName="MacOS";
	if (navigator.appVersion.indexOf("X11")!=-1) OSName="UNIX";
	if (navigator.appVersion.indexOf("Linux")!=-1) OSName="Linux";
	if (OSName=="MacOS") {
		jQuery('form#download').attr('action', 'http://softwarecdn.sendtoprint.net/software/Studio52/allinone/MyPhotoCreationsL.app.zip')
		   .find('input[type=submit]').addClass('btn-mac').removeClass('btn-win');
	};
};

jQuery(document).ready(utmtrace_StoreUtm);
jQuery(document).ready(utmtrace_SendUtm);
jQuery(document).ready(utmtrace_FormSettings);
